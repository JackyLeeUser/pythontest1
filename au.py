import numpy as np
import cv2
import dlib
def au_normalize(au_list):
    au_x = []
    au_y = []
    normalize_xi = []
    normalize_yi = []
    for i in range(len(au_list)):
        au_x.append(au_list[i][0])
    for i in range(len(au_list)):
        au_y.append(au_list[i][1])
    max_x = max(au_x)
    min_x = min(au_x)
    max_y = max(au_y)
    min_y = min(au_y)
    for i,j in zip(au_x,au_y):
        normalize_x = (i - min_x)/(max_x - min_x)
        normalize_xi.append(normalize_x)
        if (i-min_x) > 0:
                normalize_y = (j* normalize_x/(i-min_x))
                normalize_yi.append(normalize_y)
        elif (i-min_x) == 0:
            x_value = 0.00001
            normalize_y = (j* normalize_x/x_value)
            normalize_yi.append(normalize_y)
    y1_n = normalize_yi[1]
    y2_n = normalize_yi[2]
    y1 = au_y[1]
    y2 = au_y[2]
    y0 = min_y
    normalize_min_y = abs(((y0-y1)*(y2_n)+(y2-y0)*(y1_n))/2)
    normalize_yi[normalize_yi.index(min(normalize_yi))] = normalize_min_y
    sum = 0
    for i in normalize_yi:
        sum = sum + i 
    result = (sum/len(normalize_yi))
    print(result)
    return result


if __name__ == "__main__":
    au12 = [(101,60),(125,64),(136,61),(151,61),(163,62),(175,62),(198,60),(180,75),(165,80),(151,80),(137,79),(120,73),(110,61),(136,70),(151,69),(164,68),(191,64),(164,68),(151,69),(136,70)]
    au6_eyeleft = [(66,87),(86,71),(108,70),(122,83),(107,90),(88,90)]
    au6_eyeright = [(186,85),(202,68),(225,69),(244,83),(225,86),(202,87)]
    au6_blowleft = [(62,57),(73,49),(88,49),(103,50),(114,59)]
    au6_blowright = [(191,59),(202,49),(221,44),(239,44),(251,50)]
    b = (au_normalize(au12) + au_normalize(au6_eyeleft) +au_normalize(au6_eyeright) +au_normalize(au6_blowleft) +au_normalize(au6_blowright))/5
    print(b)


# max_x = 0
# min_x = 10000
# min_y = 10000
# max_y = 0
# max_x = max(max_x,au_12_x)
# max_y = max(max_y,au_12_y)
# min_x = min(min_x,au_12_x)
# min_y = min(min_y,au_12_y)

# def au_normalize():
#     max_x = 

