import numpy as np
import cv2
 
# 脸
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
face_cascade.load('/data/assets1.xml')
# 眼睛
eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')
eye_cascade.load('/data/assets2.xml')

img = cv2.imread(/data/a3.JPG)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
 
#脸
faces = face_cascade.detectMultiScale(gray, 1.2, 3)
for (x, y, w, h) in faces:
     img = cv2.rectangle(img, (x,y),(x+w, y+h), (255, 0, 0), 2)
     roi_gray = gray[y:y+h, x:x+w]
     roi_color = img[y:y+h, x:x+w]

eyes = eye_cascade.detectMultiScale(roi_gray, 1.2, 3)
eyes_x1 = 10000
eyes_y1 = 10000
eyes_x2 = -1
eyes_y2 = -1
for (ex,ey,ew,eh)in eyes:
    eyes_x1 = min(eyes_x1, ex-0.1*ex)
    eyes_y1 = min(eyes_y1, ey-0.1*ey)
    eyes_x2= max(eyes_x2, ex-0.1*ex+1.1*ew)
    eyes_y2 = max(eyes_y2, ey+0.1*ey+1.1*eh)
cv2.rectangle(roi_color, (int(eyes_x1), int(eyes_y1)),(int(eyes_x2), int(eyes_y2)), (0, 255, 0), 2)
crop_img = roi_color[int(eyes_y1):int(eyes_y2),int(eyes_x1):int(eyes_x2)]
cv2.imshow("crop_img", crop_img)
cv2.imwrite('crop.jpg', crop_img)
cv2.waitKey(0)
cv2.destroyAllWindows()




