import cv2
import random
import numpy as np
class Augmentation:
    ratio =0.7
    ratio =0.7
    low =0.5 high=3
    Value =0.5
    lalue =60
    flag =True
    flag =True
    angel =30

    def __init__(self):
        self.image = {}

    def run(self):
        self.image["horizontal_shift"] = self.horizontal_shift(img, self.ratio)
        self.image["vertical_shift"] = vertical_shift(img, self.ratio)
        self.image["brightness"] = brightness(img, self.low, self.high)
        self.image["zoom"] =  zoom(img, self.value)
        self.image["channel_shift"] = channel_shift(img, self.value)
        self.image["horizontal_flip"] = horizontal_flip(img, self.flag)
        self.image["vertical_flip"] = vertical_flip(img, self.flag)
        self.image["rotation"] = rotation(img, self.angle)




      
    def fill(self,img, h, w):
        img = cv2.resize(img, (h, w), cv2.INTER_CUBIC)
        return img
        
    def horizontal_shift(self,img, ratio=0.0):
        if ratio > 1 or ratio < 0:
            print('Value should be less than 1 and greater than 0')
            return img
        ratio = random.uniform(-ratio, ratio)
        h, w = img.shape[:2]
        to_shift = w*ratio
        if ratio > 0:
            img = img[:, :int(w-to_shift), :]
        if ratio < 0:
            img = img[:, int(-1*to_shift):, :]
        img =name1.fill(img, h, w)
        return img

    def vertical_shift(self,img, ratio=0.0):
        if ratio > 1 or ratio < 0:
            print('Value should be less than 1 and greater than 0')
            return img
        ratio = random.uniform(-ratio, ratio)
        h, w = img.shape[:2]
        to_shift = h*ratio
        if ratio > 0:
            img = img[:int(h-to_shift), :, :]
        if ratio < 0:
            img = img[int(-1*to_shift):, :, :]
        img = name1.fill(img, h, w)
        return img

    def brightness(self,img, low, high):
        value = random.uniform(low, high)
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        hsv = np.array(hsv, dtype = np.float64)
        hsv[:,:,1] = hsv[:,:,1]*value
        hsv[:,:,1][hsv[:,:,1]>255]  = 255
        hsv[:,:,2] = hsv[:,:,2]*value 
        hsv[:,:,2][hsv[:,:,2]>255]  = 255
        hsv = np.array(hsv, dtype = np.uint8)
        img = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
        return img

    def zoom(self,img, value):
        if value > 1 or value < 0:
            print('Value for zoom should be less than 1 and greater than 0')
            return img
        value = random.uniform(value, 1)
        h, w = img.shape[:2]
        h_taken = int(value*h)
        w_taken = int(value*w)
        h_start = random.randint(0, h-h_taken)
        w_start = random.randint(0, w-w_taken)
        img = img[h_start:h_start+h_taken, w_start:w_start+w_taken, :]
        img =name1.fill(img, h, w)
        return img

    def channel_shift(self,img, value):
        value = int(random.uniform(-value, value))
        img = img + value
        img[:,:,:][img[:,:,:]>255]  = 255
        img[:,:,:][img[:,:,:]<0]  = 0
        img = img.astype(np.uint8)
        return img

    def horizontal_flip(self,img, flag):
        if flag:
            return cv2.flip(img, 1)
        else:
            return img

    def vertical_flip(self,img, flag):
        if flag:
            return cv2.flip(img, 0)
        else:
            return img

    def rotation(self,img, angle):
        angle = int(random.uniform(-angle, angle))
        h, w = img.shape[:2]
        M = cv2.getRotationMatrix2D((int(w/2), int(h/2)), angle, 1)
        img = cv2.warpAffine(img, M, (w, h))
        return img
            


if __name__ == '__main__':
        img = cv2.imread('a1.jpeg')
        name1 = Augmentation()
        #img= name1.horizontal_shift(img, 0.7)
        #img =name1.vertical_shift(img, 0.7)
        #img = name1.brightness(img, 0.5, 3)
        #img = name1.zoom(img, 0.5)
        #img = name1.channel_shift(img, 60)
        #img = name1.horizontal_flip(img, True)
        #img = name1.vertical_flip(img, True)
        #img = name1.rotation(img, 30)

        cv2.imshow('Result', images.get("horizontal_shift"))
        cv2.waitKey(0)
        cv2.destroyAllWindows()





