class Score:
    def normalize(self,img,start_idx,end_idx):
        # 取灰度
        img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        # 人臉數rects
        rects = detector(img_gray, 0)
        marks = None
        self.start_idx = start_idx
        self.end_idx = end_idx
        for i in range(len(rects)):
            landmarks = np.matrix([[p.x, p.y] for p in predictor(img,rects[i]).parts()])
            marks = landmarks[self.start_idx:self.end_idx]
        if marks is None:
            print("Not Detect Mouth in Image.")
            return False

        max_x = 0
        min_x = 10000
        min_y = 10000
        max_y = 0
        # max_value = [0, 0]
        # min_value = [10000, 10000]
        
        normalize_xi = []
        y = []
        normalize_yi = []
        for idx, point in enumerate(marks):
            # 68點的座標
            pos = (point[0, 0], point[0, 1])
            y.append(pos[1])
            # 利用cv2.circle給每個特徵點畫一個圈，共68個
            cv2.circle(img, pos, 5, color=(0, 255, 0))
            # 利用cv2.putText輸出1-68
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(img, str(idx+1), pos, font, 0.8, (0, 0, 255), 1,cv2.LINE_AA)
            max_x = max(max_x,pos[0])
            max_y = max(max_y,pos[1])
            min_x = min(min_x,pos[0])
            min_y = min(min_y,pos[1])
        for idx, point in enumerate(marks):
            pos = (point[0, 0], point[0, 1])
            normalize_x = (pos[0] - min_x)/(max_x - min_x)
            normalize_xi.append(normalize_x)
            if (pos[0]-min_x) > 0:
                normalize_y = (pos[1]* normalize_x/(pos[0]-min_x))
                normalize_yi.append(normalize_y)
            elif (pos[0]-min_x) == 0:
                x_value = 0.00001
                normalize_y = (pos[1]* normalize_x/x_value)
                normalize_yi.append(normalize_y)
        y1_n = normalize_yi[1]
        y2_n = normalize_yi[2]
        y1 = y[1]
        y2 = y[2]
        y0 = min(y)
        normalize_min_y = abs(((y0-y1)*(y2_n)+(y2-y0)*(y1_n))/2)
        normalize_yi[normalize_yi.index(min(normalize_yi))] = normalize_min_y
        sum = 0
        for i in normalize_yi:
            sum = sum + i 
        result = (sum/len(normalize_yi))
        return result

    def sub(self,a,b):
        result = abs(a - b)
        return result  

if __name__ == "__main__":
    import numpy as np
    import cv2
    import dlib
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor('/Users/hou/Downloads/shape_predictor_68_face_landmarks.dat')

    input_img_path = './a6.PNG'
    input_img = cv2.imread(input_img_path)
    mouth_input_score = Score()
    eyeleft_input_score = Score()
    eyeright_input_score = Score()
    eyeblow_left_input_score = Score()
    eyeblow_right_input_score = Score()
    
    mouth_input_value = mouth_input_score.normalize(input_img,48,68)
    eyeleft_input_value = eyeleft_input_score.normalize(input_img,37,42)
    eyeright_input_value = eyeright_input_score.normalize(input_img,43,47)
    eyeblow_left_input_value = eyeblow_left_input_score.normalize(input_img,18,22)
    eyeblow_right_input_value = eyeblow_right_input_score.normalize(input_img,23,27)


    au_img_path = ['./a4.jpg','./a3.JPG','./a8.jpg']
    mouth_list = []
    eyeleft_list = []
    eyeright_list = []
    eyeblow_left_list = []
    eyeblow_right_list = []

    mouth_value = Score()
    eyeleft_value = Score()
    eyeright_value = Score()
    eyeblow_left_value = Score()
    eyeblow_right_value = Score()

    for i in range(len(au_img_path)):
        img = cv2.imread(au_img_path[i])
        mouth_output_value = mouth_value.normalize(img,48,68)

        eyeleft_output_value = eyeleft_value.normalize(img,37,42)
        eyeright_output_value = eyeright_value.normalize(img,43,47)
        eyeblow_left_output_value = eyeblow_left_value.normalize(img,18,22)
        eyeblow_right_output_value = eyeblow_right_value.normalize(img,23,27)

        mouth_list.append(mouth_output_value)
        eyeleft_list.append(eyeleft_output_value)
        eyeright_list.append(eyeright_output_value)
        eyeblow_left_list.append(eyeblow_left_output_value)
        eyeblow_right_list.append(eyeblow_right_output_value)

    mouth_score = []
    mouth_sub = Score()
    for i in range(len(mouth_list)):
        diff = mouth_sub.sub(mouth_input_value, mouth_list[i])
        mouth_score.append(diff)
    if mouth_score[0] == min(mouth_score):
        print("happy")
    elif mouth_score[1] == min(mouth_score):
        print("angry")
    elif mouth_score[2] == min(mouth_score):
        print("sad")


# landmark_dict = [
#     {
#         'name' : "mouth"
#         'start_idx' : 48,
#         'end_idx' : 68
#     },
#     {
#         'name' : "left_eyes"
#         'start_idx' : 0,
#         'end_idx' : 12
#     }
#     ...
# ]

# for value in landmark_dict:
#     value['start_idx']