import numpy as np
import cv2
import dlib

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('/Users/hou/Downloads/shape_predictor_68_face_landmarks.dat')

# cv2讀取影象
img = cv2.imread("a6.PNG")
#img = cv2.imread("a4.jpg")
#img = cv2.imread("a3.JPG")

# 取灰度
img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

# 人臉數rects
rects = detector(img_gray, 0)

for i in range(len(rects)):
    landmarks = np.matrix([[p.x, p.y] for p in predictor(img,rects[i]).parts()])
    mouth_marks = landmarks[48:68]
    max_x = 0
    min_x = 10000
    min_y = 10000
    max_y = 0
    # max_value = [0, 0]
    # min_value = [10000, 10000]
    for idx, point in enumerate(mouth_marks):
        # 68點的座標
        pos = (point[0, 0], point[0, 1])
        # 利用cv2.circle給每個特徵點畫一個圈，共68個
        cv2.circle(img, pos, 5, color=(0, 255, 0))
        # 利用cv2.putText輸出1-68
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(img, str(idx+1), pos, font, 0.8, (0, 0, 255), 1,cv2.LINE_AA)
        max_x = max(max_x,pos[0])
        max_y = max(max_y,pos[1])
        min_x = min(min_x,pos[0])
        min_y = min(min_y,pos[1])
    for idx, point in enumerate(mouth_marks):
        pos = (point[0, 0], point[0, 1])
        b = (pos[1] - min_y)/(max_y - min_y)
        #print(b)
y1_n = 3.092009685230024
y2_n = 3.0968523002421304
y1 = 1277
y2 = 1279
y0 = 1260
y0_n = ((y0-y1)*(y2_n)+(y2-y0)*(y1_n))/2
print(mouth_marks[0])  

        
    #print(max_x)
    #print(max_y) 





# face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
# face_cascade.load('/Users/hou/Downloads/opencv-master/data/haarcascades/haarcascade_frontalface_default.xml')
# mouth_cascade = cv2.CascadeClassifier('haarcascade_mcs_mouth.xml')
# mouth_cascade.load('/Users/hou/Downloads/OpenCV_Build_Arm-4d215c43800e8d01345b496747bd167803f6ffc0/build/share/OpenCV/haarcascades/haarcascade_mcs_mouth.xml')
# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


# #脸
# faces = face_cascade.detectMultiScale(gray, 1.2, 3)
# for (x, y, w, h) in faces:
#     cv2.rectangle(img, (x,y),(x+w, y+h), (255, 0, 0), 2)
#     roi_gray = gray[y:y+h, x:x+w]
#     roi_color = img[y:y+h, x:x+w]

# #cv2.namedWindow("img", 2)
# cv2.imshow("img",img)
# cv2.waitKey(0)