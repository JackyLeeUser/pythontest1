import numpy as np
import cv2
import dlib
#class Mouth():
def show_face(img):
    # 取灰度
    img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    # 人臉數rects
    rects = detector(img_gray, 0)
    mouth_marks = None
    mouth_start_idx = 48
    mouth_end_idx = 68
    for i in range(len(rects)):
        landmarks = np.matrix([[p.x, p.y] for p in predictor(img,rects[i]).parts()])
        mouth_marks = landmarks[mouth_start_idx:mouth_end_idx]
    max_x = 0
    min_x = 10000
    min_y = 10000
    max_y = 0
    # max_value = [0, 0]
    # min_value = [10000, 10000]
    
    mouth_normalize_x = []
    mouth_y = []
    mouth_normalize_y = []
    for idx, point in enumerate(mouth_marks):
        # 68點的座標
        pos = (point[0, 0], point[0, 1])
        mouth_y.append(pos[1])
        # 利用cv2.circle給每個特徵點畫一個圈，共68個
        cv2.circle(img, pos, 5, color=(0, 255, 0))
        # 利用cv2.putText輸出1-68
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(img, str(idx+1), pos, font, 0.8, (0, 0, 255), 1,cv2.LINE_AA)
        max_x = max(max_x,pos[0])
        max_y = max(max_y,pos[1])
        min_x = min(min_x,pos[0])
        min_y = min(min_y,pos[1])
    for idx, point in enumerate(mouth_marks):
        pos = (point[0, 0], point[0, 1])
        normalize_x = (pos[0] - min_x)/(max_x - min_x)
        mouth_normalize_x.append(normalize_x)
        if (pos[0]-min_x) > 0:
            normalize_y = (pos[1]* normalize_x/(pos[0]-min_x))
            mouth_normalize_y.append(normalize_y)
    #print(mouth_z)  
    #print(normalize_x) 
    y1_n = mouth_normalize_y[0]
    y2_n = mouth_normalize_y[1]
    y1 = mouth_y[1]
    y2 = mouth_y[2]
    y0 = mouth_y[0]
    normalize_min_y = abs(((y0-y1)*(y2_n)+(y2-y0)*(y1_n))/2)
    mouth_normalize_y.append(normalize_min_y)
    #print(mouth_x , mouth_z , normalize_min_y)

    return (mouth_normalize_x , mouth_normalize_y)
def avg(nb):
    sum = 0
    for i in nb:
        sum = sum + i 
    result = (sum/len(nb))
#print(result)
    return result    



detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('/Users/hou/Downloads/shape_predictor_68_face_landmarks.dat')

    # cv2讀取影象
img = cv2.imread("a6.PNG")
x0, y0, = show_face(img)
avg_y1 = avg(y0)

img2 = cv2.imread("a4.jpg")
x1, y1, = show_face(img2)
avg_y2 = avg(y1)
print(abs(avg_y1 - avg_y2))

#b = mouth_x
#print(a)
#b = normalize_x




    #b = normalize_x 
    #print(a-b)

#img = cv2.imread("a3.JPG")

# # 取灰度
# img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

# # 人臉數rects
# rects = detector(img_gray, 0)

# for i in range(len(rects)):
#     landmarks = np.matrix([[p.x, p.y] for p in predictor(img,rects[i]).parts()])
#     mouth_marks = landmarks[48:68]
#     max_x = 0
#     min_x = 10000
#     min_y = 10000
#     max_y = 0
#     # max_value = [0, 0]
#     # min_value = [10000, 10000]
#     mouth_y = []
#     mouth_z = []
#     for idx, point in enumerate(mouth_marks):
#         # 68點的座標
#         pos = (point[0, 0], point[0, 1])
#         mouth_y.append(pos[1])
#         # 利用cv2.circle給每個特徵點畫一個圈，共68個
#         cv2.circle(img, pos, 5, color=(0, 255, 0))
#         # 利用cv2.putText輸出1-68
#         font = cv2.FONT_HERSHEY_SIMPLEX
#         cv2.putText(img, str(idx+1), pos, font, 0.8, (0, 0, 255), 1,cv2.LINE_AA)
#         max_x = max(max_x,pos[0])
#         max_y = max(max_y,pos[1])
#         min_x = min(min_x,pos[0])
#         min_y = min(min_y,pos[1])
#     for idx, point in enumerate(mouth_marks):
#         pos = (point[0, 0], point[0, 1])
#         normalize_x = (pos[0] - min_x)/(max_x - min_x)
#         normalize_y = (pos[1]* normalize_x/(pos[0]-min_x))
#         mouth_z.append(normalize_y)
#         print(normalize_y)   
#     #print(mouth_marks) 
#     y1_n = mouth_z[1]
#     y2_n = mouth_z[2]
#     y1 = mouth_y[1]
#     y2 = mouth_y[2]
#     y0 = mouth_y[0]
#     normalize_min_y = ((y0-y1)*(y2_n)+(y2-y0)*(y1_n))/2
    #print(normalize_min_y )    

        
    #print(max_x)
    #print(max_y) 





# face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
# face_cascade.load('/Users/hou/Downloads/opencv-master/data/haarcascades/haarcascade_frontalface_default.xml')
# mouth_cascade = cv2.CascadeClassifier('haarcascade_mcs_mouth.xml')
# mouth_cascade.load('/Users/hou/Downloads/OpenCV_Build_Arm-4d215c43800e8d01345b496747bd167803f6ffc0/build/share/OpenCV/haarcascades/haarcascade_mcs_mouth.xml')
# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


# #脸
# faces = face_cascade.detectMultiScale(gray, 1.2, 3)
# for (x, y, w, h) in faces:
#     cv2.rectangle(img, (x,y),(x+w, y+h), (255, 0, 0), 2)
#     roi_gray = gray[y:y+h, x:x+w]
#     roi_color = img[y:y+h, x:x+w]

cv2.namedWindow("img", 2)
cv2.imshow("img",img)
cv2.waitKey(0)